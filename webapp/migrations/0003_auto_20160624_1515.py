# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-24 09:45
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('doccon2', '0002_extractedanswers_solr_added'),
    ]

    operations = [
        migrations.AddField(
            model_name='converted',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2016, 6, 24, 9, 45, 39, 843000, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='converted',
            name='extracted',
            field=models.CharField(choices=[('YES', 'Y'), ('NO', 'N')], default='N', max_length=3, verbose_name='Extracted?'),
        ),
    ]
